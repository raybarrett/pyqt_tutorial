import sys
from PyQt4 import QtGui


class WindowAction:
        NEW = 'New'
        CASC = 'Cascade'
        TILE = 'Tiled'


class MainWindow(QtGui.QMainWindow):
    count = 0

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.mdi = QtGui.QMdiArea()
        self.setCentralWidget(self.mdi)
        bar = self.menuBar()

        file = bar.addMenu("File")
        file.addAction(WindowAction.NEW)
        file.addAction(WindowAction.CASC)
        file.addAction(WindowAction.TILE)
        file.triggered[QtGui.QAction].connect(self.windowaction)
        self.setWindowTitle("MDI demo")

    def windowaction(self, q):
        print "triggered"

        if q.text() == WindowAction.NEW:
            MainWindow.count = MainWindow.count + 1
            sub = QtGui.QMdiSubWindow()
            sub.setWidget(QtGui.QTextEdit())
            sub.setWindowTitle("subwindow" + str(MainWindow.count))
            self.mdi.addSubWindow(sub)
            sub.show()

        if q.text() == WindowAction.CASC:
            self.mdi.cascadeSubWindows()

        if q.text() == WindowAction.TILE:
            self.mdi.tileSubWindows()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
