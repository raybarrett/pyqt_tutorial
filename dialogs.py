import sys
from PyQt4 import QtGui
from PyQt4.QtCore import Qt


def window():
    app = QtGui.QApplication(sys.argv)
    w = QtGui.QWidget()
    b = QtGui.QPushButton(w)
    b.setText("Hello World!")
    b.move(50, 50)
    b.clicked.connect(showdialog)
    w.setWindowTitle("PyQt Dialog demo")
    w.show()
    sys.exit(app.exec_())


def showdialog():
    d = QtGui.QDialog()
    b1 = QtGui.QPushButton("ok", d)
    b1.move(50, 50)
    d.setWindowTitle("Dialog")
    d.setWindowModality(Qt.ApplicationModal)
    d.exec_()


if __name__ == '__main__':
    window()
